<?php

declare(strict_types=1);

namespace SlyFoxCreative\Usaepay\Gateway\Validator;

use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Validator\AbstractValidator;

class RefundResponseValidator extends AbstractValidator
{
    public function validate(array $validationSubject)
    {
        $response = SubjectReader::readResponse($validationSubject)[0];

        if ($response->ErrorCode === 0 || $response->ErrorCode === null) {
            return $this->createResult(true, []);
        }

        return $this->createResult(false, ['Gateway rejected the transaction.'], [$response->ErrorCode]);
    }
}
