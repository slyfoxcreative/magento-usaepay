<?php

declare(strict_types=1);

namespace SlyFoxCreative\Usaepay\Gateway\Validator;

use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Validator\AbstractValidator;

class SaleResponseValidator extends AbstractValidator
{
    private $avsResultCodes = ['YYY', 'YYX', 'NYZ', 'NYW', 'YNA'];

    public function validate(array $validationSubject)
    {
        $response = SubjectReader::readResponse($validationSubject)[0];

        if ($response->ErrorCode === 0) {
            return $this->createResult(true, []);
        }

        if ($response->CardCodeResultCode !== 'M') {
            return $this->createResult(false, ['CVV mismatch.'], ['cvv']);
        }

        if (!in_array($response->AvsResultCode, $this->avsResultCodes)) {
            return $this->createResult(false, ['AVS failed.'], ['avs']);
        }

        return $this->createResult(false, ['Gateway rejected the transaction.'], [$response->ErrorCode]);
    }
}
