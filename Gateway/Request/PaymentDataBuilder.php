<?php

declare(strict_types=1);

namespace SlyFoxCreative\Usaepay\Gateway\Request;

use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Payment\Helper\Formatter;

class PaymentDataBuilder implements BuilderInterface
{
    use Formatter;

    public function build(array $buildSubject)
    {
        $subject = SubjectReader::readPayment($buildSubject);
        $payment = $subject->getPayment();
        $order = $subject->getOrder();
        $address = $order->getBillingAddress();

        $month = str_pad($payment->getAdditionalInformation('cc_exp_month'), 2, '0', STR_PAD_LEFT);
        $year = mb_substr($payment->getAdditionalInformation('cc_exp_year'), -2);
        $expiration = "{$month}{$year}";

        return [
            'AccountHolder' => $address->getFirstName() . ' ' . $address->getLastName(),
            'CreditCardData' => [
                'CardCode' => $payment->getAdditionalInformation('cc_cid'),
                'CardNumber' => $payment->getAdditionalInformation('cc_number'),
                'CardExpiration' => $expiration,
                'AvsStreet' => $address->getStreetLine1(),
                'AvsZip' => $address->getPostcode(),
            ],
            'Details' => [
                'Amount' => $this->formatPrice(SubjectReader::readAmount($buildSubject)),
                'OrderID' => $order->getOrderIncrementId(),
            ],
        ];
    }
}
