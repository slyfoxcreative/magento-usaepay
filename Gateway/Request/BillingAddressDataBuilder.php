<?php

declare(strict_types=1);

namespace SlyFoxCreative\Usaepay\Gateway\Request;

use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Request\BuilderInterface;

class BillingAddressDataBuilder implements BuilderInterface
{
    public function build(array $buildSubject)
    {
        $address = SubjectReader::readPayment($buildSubject)->getOrder()->getBillingAddress();

        return [
            'BillingAddress' => [
                'City' => $address->getCity(),
                'Company' => $address->getCompany(),
                'Country' => $address->getCountryId(),
                'FirstName' => $address->getFirstName(),
                'Lastname' => $address->getLastName(),
                'Phone' => $address->getTelephone(),
                'State' => $address->getRegionCode(),
                'Street' => $address->getStreetLine1(),
                'Street2' => $address->getStreetLine2(),
                'Zip' => $address->getPostcode(),
            ],
        ];
    }
}
