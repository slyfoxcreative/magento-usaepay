<?php

declare(strict_types=1);

namespace SlyFoxCreative\Usaepay\Gateway\Request;

use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Payment\Helper\Formatter;

class RefundDataBuilder implements BuilderInterface
{
    use Formatter;

    public function build(array $buildSubject)
    {
        $subject = SubjectReader::readPayment($buildSubject);
        $payment = $subject->getPayment();

        return [
            'RefNum' => $payment->getParentTransactionId(),
            'Amount' => $this->formatPrice(SubjectReader::readAmount($buildSubject)),
        ];
    }
}
