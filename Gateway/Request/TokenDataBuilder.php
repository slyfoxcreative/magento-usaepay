<?php

declare(strict_types=1);

namespace SlyFoxCreative\Usaepay\Gateway\Request;

use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Request\BuilderInterface;
use SlyFoxCreative\Usaepay\Gateway\Config\Config;

class TokenDataBuilder implements BuilderInterface
{
    private $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function build(array $buildSubject)
    {
        $order = SubjectReader::readPayment($buildSubject)->getOrder();

        $key = $this->config->getKey($order->getStoreId());
        $pin = $this->config->getPin($order->getStoreId());
        $seed = base64_encode(random_bytes(20));
        $hash = hash('sha1', "{$key}{$seed}{$pin}");

        return [
            'Token' => [
                'SourceKey' => $key,
                'PinHash' => [
                    'Type' => 'sha1',
                    'Seed' => $seed,
                    'HashValue' => $hash,
                ],
                'ClientIP' => $_SERVER['REMOTE_ADDR'],
            ],
        ];
    }
}
