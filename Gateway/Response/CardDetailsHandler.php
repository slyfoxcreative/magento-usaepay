<?php

declare(strict_types=1);

namespace SlyFoxCreative\Usaepay\Gateway\Response;

use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Response\HandlerInterface;

class CardDetailsHandler implements HandlerInterface
{
    public function handle(array $handlingSubject, array $response)
    {
        $response = $response[0];

        $subject = SubjectReader::readPayment($handlingSubject);
        $payment = $subject->getPayment();

        $number = mb_substr($payment->getAdditionalInformation('cc_number'), -4);
        $type = $this->type($payment->getAdditionalInformation('cc_type'));

        $payment->setCcLast4($number);
        $payment->setCcType($type);
        $payment->setCcExpMonth($payment->getAdditionalInformation('cc_exp_month'));
        $payment->setCcExpYear($payment->getAdditionalInformation('cc_exp_year'));

        $payment->unsAdditionalInformation('cc_cid');
        $payment->unsAdditionalInformation('cc_start_month');
        $payment->unsAdditionalInformation('cc_start_year');
        $payment->unsAdditionalInformation('cc_ss_issue');
        $payment->setAdditionalInformation('cc_number', $number);
        $payment->setAdditionalInformation('cc_type', $type);
    }

    private function type(string $code)
    {
        switch ($code) {
        case 'AE':
            return 'American Express';
        case 'DI':
            return 'Discover';
        case 'MC':
            return 'MasterCard';
        case 'VI':
            return 'Visa';
        }
    }
}
