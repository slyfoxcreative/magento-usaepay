<?php

declare(strict_types=1);

namespace SlyFoxCreative\Usaepay\Gateway\Response;

use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Response\HandlerInterface;

class PaymentDetailsHandler implements HandlerInterface
{
    protected $additionalInformationMapping = [
        'AvsResultCode',
        'AvsResult',
        'CardCodeResultCode',
        'CardCodeResult',
        'CardLevelResultCode',
        'CardLevelResult',
        'ResultCode',
        'Result',
    ];

    public function handle(array $handlingSubject, array $response)
    {
        $response = $response[0];

        $subject = SubjectReader::readPayment($handlingSubject);
        $payment = $subject->getPayment();

        foreach ($this->additionalInformationMapping as $item) {
            if (!isset($response->{$item})) {
                continue;
            }

            $payment->setAdditionalInformation($item, $response->{$item});
        }
    }
}
