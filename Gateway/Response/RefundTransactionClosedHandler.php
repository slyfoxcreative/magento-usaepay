<?php

declare(strict_types=1);

namespace SlyFoxCreative\Usaepay\Gateway\Response;

use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Response\HandlerInterface;

class RefundTransactionClosedHandler implements HandlerInterface
{
    public function handle(array $handlingSubject, array $response)
    {
        $response = $response[0];

        $subject = SubjectReader::readPayment($handlingSubject);
        $payment = $subject->getPayment();

        $payment->setIsTransactionClosed(true);
    }
}
