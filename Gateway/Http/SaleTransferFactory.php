<?php

declare(strict_types=1);

namespace SlyFoxCreative\Usaepay\Gateway\Http;

class SaleTransferFactory extends TransferFactory
{
    protected function method()
    {
        return 'runSale';
    }

    protected function arguments(array $arguments)
    {
        return [$arguments];
    }
}
