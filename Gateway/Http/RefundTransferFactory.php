<?php

declare(strict_types=1);

namespace SlyFoxCreative\Usaepay\Gateway\Http;

class RefundTransferFactory extends TransferFactory
{
    protected function method()
    {
        return 'refundTransaction';
    }

    protected function arguments(array $arguments)
    {
        return [
            $arguments['RefNum'],
            $arguments['Amount'],
        ];
    }
}
