<?php

declare(strict_types=1);

namespace SlyFoxCreative\Usaepay\Gateway\Http;

use Magento\Payment\Gateway\Http\TransferBuilder;
use Magento\Payment\Gateway\Http\TransferFactoryInterface;
use SlyFoxCreative\Usaepay\Gateway\Config\Config;

abstract class TransferFactory implements TransferFactoryInterface
{
    private $config;
    private $transferBuilder;

    public function __construct(Config $config, TransferBuilder $transferBuilder)
    {
        $this->config = $config;
        $this->transferBuilder = $transferBuilder;
    }

    public function create(array $request)
    {
        $uri = $this->config->getEnvironment() === 'production'
            ? 'https://www.usaepay.com/soap/gate/0AE595C1/usaepay.wsdl'
            : 'https://sandbox.usaepay.com/soap/gate/0AE595C1/usaepay.wsdl';

        $token = $request['Token'];
        unset($request['Token']);

        $arguments = $this->arguments($request);
        array_unshift($arguments, $token);

        return $this->transferBuilder
            ->setUri($uri)
            ->setMethod($this->method())
            ->setBody($arguments)
            ->build()
        ;
    }

    abstract protected function method();

    abstract protected function arguments(array $arguments);
}
