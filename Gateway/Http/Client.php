<?php

declare(strict_types=1);

namespace SlyFoxCreative\Usaepay\Gateway\Http;

use Exception;
use Magento\Framework\Webapi\Soap\ClientFactory;
use Magento\Payment\Gateway\Http\ClientInterface;
use Magento\Payment\Gateway\Http\TransferInterface;
use Magento\Payment\Model\Method\Logger;
use Psr\Log\LoggerInterface;

class Client implements ClientInterface
{
    private $logger;
    private $customLogger;
    private $clientFactory;

    public function __construct(LoggerInterface $logger, Logger $customLogger, ClientFactory $clientFactory)
    {
        $this->logger = $logger;
        $this->customLogger = $customLogger;
        $this->clientFactory = $clientFactory;
    }

    public function placeRequest(TransferInterface $transferObject)
    {
        $this->customLogger->debug([
            'uri' => $transferObject->getUri(),
            'method' => $transferObject->getMethod(),
            'request' => $transferObject->getBody(),
        ]);

        $client = $this->clientFactory->create(
            $transferObject->getUri(),
            ['trace' => true]
        );

        try {
            $response = $client->__soapCall($transferObject->getMethod(), $transferObject->getBody());
        } catch (Exception $e) {
            $this->logger->critical($client->__getLastRequest());
            $this->logger->critical($client->__getLastResponse());

            throw $e;
        }

        $this->customLogger->debug(['response' => $response]);

        return [$response];
    }
}
