<?php

declare(strict_types=1);

namespace SlyFoxCreative\Usaepay\Gateway\Config;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Payment\Gateway\Config\Config as MagentoConfig;

class Config extends MagentoConfig
{
    private $serializer;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        $methodCode = null,
        $pathPattern = self::DEFAULT_PATH_PATTERN,
        Json $serializer = null
    ) {
        parent::__construct($scopeConfig, $methodCode, $pathPattern);
        $this->serializer = $serializer ?: ObjectManager::getInstance()->get(Json::class);
    }

    public function isActive($storeId = null)
    {
        return (bool) $this->getValue('active', $storeId);
    }

    public function getEnvironment($storeId = null)
    {
        return $this->getValue('environment', $storeId);
    }

    public function getKey($storeId = null)
    {
        return $this->getValue('key', $storeId);
    }

    public function getPin($storeId = null)
    {
        return $this->getValue('pin', $storeId);
    }
}
