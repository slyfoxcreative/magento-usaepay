<?php

namespace SlyFoxCreative\Usaepay\Model\Ui;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\Session\SessionManagerInterface;
use SlyFoxCreative\Usaepay\Gateway\Config\Config;

final class ConfigProvider implements ConfigProviderInterface
{
    const CODE = 'usaepay';

    private $config;
    private $session;

    public function __construct(Config $config, SessionManagerInterface $session)
    {
        $this->config = $config;
        $this->session = $session;
    }

    public function getConfig()
    {
        $storeId = $this->session->getStoreId();

        return [
            'payment' => [
                self::CODE => [
                    'isActive' => $this->config->isActive($storeId),
                ],
            ],
        ];
    }
}
