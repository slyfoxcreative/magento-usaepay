<?php

declare(strict_types=1);

namespace SlyFoxCreative\Usaepay\Model\Adminhtml\Source;

use Magento\Framework\Option\ArrayInterface;

class Environment implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
            [
                'value' => 'production',
                'label' => 'Production',
            ],
            [
                'value' => 'sandbox',
                'label' => 'Sandbox',
            ],
        ];
    }
}
