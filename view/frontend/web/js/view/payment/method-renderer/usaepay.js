define(
    [
        'Magento_Payment/js/view/payment/cc-form',
    ],
    function (Component) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'SlyFoxCreative_Usaepay/payment/form'
            },

            getCode: function () {
                return 'usaepay';
            },
        });
    }
)
