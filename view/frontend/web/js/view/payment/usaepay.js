define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (Component, rendererList) {
        'use strict';

        var config = window.checkoutConfig.payment;

        if (config['usaepay'].isActive) {
            rendererList.push({
                type: 'usaepay',
                component: 'SlyFoxCreative_Usaepay/js/view/payment/method-renderer/usaepay'
            });
        }

        return Component.extend({});
    }
)
