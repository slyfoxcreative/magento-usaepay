<?php

declare(strict_types=1);

namespace SlyFoxCreative\Usaepay\Block;

use Magento\Payment\Block\ConfigurableInfo;

class Info extends ConfigurableInfo
{
    protected function getLabel($field)
    {
        return __($field);
    }
}
